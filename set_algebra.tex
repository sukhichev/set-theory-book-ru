\chapter{Алгебра множеств}
\label{p:set_algebra}
Ранее множества у нас появлялись сразу как факт. Однако новые множества можно получать и из других, уже известных нам множеств. Для этого вводят операции над множествами. Для операций исходными данными являются одно или несколько множеств и результатом является одно множество.

Для иллюстрации операций над множествами традиционно используют диаграммы Эйлера"--~Венна. На этих диаграммах исходные множества изображаются геометрическими фигурами (обычно кругами или прямоугольниками), а результат выделяется цветом или штриховкой как изображено на рис.~\ref{img:euler-venn_diagram}.

\begin{figure}
\begin{center}
  \subfloat[]{
  \includegraphics[width=0.45\linewidth]{evd_union}
  \label{img:evd_union}
  }\hfill
  \subfloat[]{
  \includegraphics[width=0.45\linewidth]{evd_intersection}
  \label{img:evd_intersection}
  }

  \subfloat[]{
  \includegraphics[width=0.45\linewidth]{evd_rel_complement}
  \label{img:evd_rel_complement}
  }\hfill
  \subfloat[]{
  \includegraphics[width=0.45\linewidth]{evd_symmetric_difference}
  \label{img:evd_symmetric_difference}
  }

  \subfloat[]{
  \includegraphics[width=0.6\linewidth]{evd_abs_complement}
  \label{img:evd_abs_complement}
  }
 \caption{Диаграммы Эйлера"--~Венна:\protect\\ 
\protect\subref{img:evd_union} "--- объединение;
\protect\subref{img:evd_intersection} "--- пересечение;
\protect\subref{img:evd_rel_complement} "--- разность;\protect\\ 
\protect\subref{img:evd_symmetric_difference} "--- симметрическая разность;
\protect\subref{img:evd_abs_complement} "--- дополнение.}
\label{img:euler-venn_diagram} 
\end{center}
\end{figure}

Над множествами определены следующие операции:
\begin{description}
\item [Объединение (рис.~\ref{img:evd_union}).] Результат этой операции содержит все элементы составляющих его множеств. Объединение множеств \set{A} и \set{B} в виде формулы записывается как $\set{A} \cup \set{B}$. Для обозначения объединения группы множеств \set{A_i} будет использоваться запись $ \bigcup_{i=m}^{n} \set{A_i} $, где $ m $ и $ n $ "--- пределы изменения целочисленного индекса~$ i $.  Иногда в литературе встречается обозначение $\set{A} + \set{B}$, которое в этой книге использоваться не будет, чтобы не было путаницы с арифметическим сложением.

Заметим, что операция объединения является просто сокращения последовательного применения аксиом пары и объединения для двух или более множеств (см. раздел~\ref{p:construct_axioms}).

\item [Пересечение (рис.~\ref{img:evd_intersection}).] Результат содержит все элементы, являющиеся общими для всех исходных множеств. Тогда пересечение множеств \set{A} и \set{B} можно записать как $\set{A} \cap \set{B}$. Аналогично объединению для обозначения пересечения группы множеств \set{A_i} будет использоваться запись $ \bigcap_{i=m}^{n} \set{A_i} $, где $ i $, $ m $ и $ n $ имеют те же значения. Кроме того изредка встречается обозначение $\set{A} \, \set{B}$\footnote{В компьютерном наборе здесь используют четверная шпация, а не просто опускают знак.} по аналогии с арифметическим умножением, и которая в этой книге использоваться не будет именно по этой причине.

\item [Разность (рис.~\ref{img:evd_rel_complement}).] Результат содержит все элементы первого множества, которые не являются в то же время элементами другого множества. Разность множеств \set{A} и \set{B} записывается как $ \set{A} \setminus \set{B} $, хотя иногда используется обозначение $ \set{A} - \set{B} $ и даже $ \set{A} \sim \set{B} $, которые не будут использоваться по тем же причинам, что и в предыдущих случаях.

\item [Симметрическая разность (рис.~\ref{img:evd_symmetric_difference}).] Результат этой операции содержит все элементы исходных множеств, не принадлежащие одновременно обоим исходным множествам. Симметрическая разность множеств \set{A} и \set{B} записывается как $ \set{A} \setsymdiff \set{B} $, хотя иногда встречается обозначение $ \set{A} \,\dot{-}\,\set{B} $, которое очень редко используется, т.\,к. его, не заметив точку, можно перепутать с обычной разностью.

\item  [Дополнение (рис.~\ref{img:evd_abs_complement}).] Результат содержит все элементы универсума, которые не входят в множество над которым производят дополнение. Фактически это разность между универсумом и множеством над которым производят дополнение.

Дополнение "--- это единственная операция над множествами, для которой требуется универсум. Так же это формальное единственная\footnote{На самом деле ещё существует такая унарная операция как <<булеан>>, которая не рассматривается в этом курсе.} унарная операция, т.\,е. операция, требующая одного аргумента.

Дополнение обозначается по-разному: чертой над переменной ($ \overline{\set{A}} $); как разность, часто опуская значок универсума ($\set{U} \setminus \set{A} $ или просто $ \setminus \set{A} $);  в виде оператора дополнения ($\set{A}^\complement$ или $ \set{A}$). 

В данной книге используется только первый вариант как наиболее короткий и наглядный.
\end{description}

Уже из этих определений видны некоторые соотношения между этими операциями. Например
\begin{equation}
\set{A} \setminus \set{B} = \set{A} \cap  \overline{\set{A}},
\end{equation}
или
\begin{equation}
\set{A} \setsymdiff \set{B} = \left( \set{A} \cup \set{B} \right) \setminus \left( \set{A} \cap \set{B} \right) .
\end{equation}

Однако, всё по порядку. Для начала рассмотрим свойства бинарных операций над множествами, т.\,е. операций, требующих двух аргументов. Это все рассмотренные ранее операции, кроме дополнения.

Далее будут даны только конечные формулы без доказательств по причине тривиальности самых доказательств.

Все бинарные операции, кроме разности, коммутативны. То есть
\begin{equation}
\set{A} \cup \set{B} = \set{B} \cup \set{A},
\end{equation}
\begin{equation}
\set{A} \cap \set{B} = \set{B} \cap \set{A},
\end{equation}
\begin{equation}
\set{A} \setsymdiff \set{B} = \set{B} \setsymdiff \set{A}.
\end{equation}

Кроме того, эти же операции ассоциативны. Отсюда
\begin{equation}
\left ( \set{A} \cup \set{B} \right) \cup \set{C} = \set{A} \cup \left ( \set{B} \cup \set{C} \right),
\end{equation}
\begin{equation}
\left ( \set{A} \cap \set{B} \right) \cap \set{C} = \set{A} \cap \left ( \set{B} \cap \set{C} \right),
\end{equation}
\begin{equation}
\left ( \set{A} \setsymdiff \set{B} \right) \setsymdiff \set{C} = \set{A} \setsymdiff \left ( \set{B} \setsymdiff \set{C} \right).
\end{equation}

Разность же множеств некоммутативна по определению и не ассоциативна:
\begin{equation}
\set{A} \setminus \left ( \set{B} \setminus \set{C} \right) = \left ( \set{A} \setminus \set{B} \right) \cup \left ( \set{A} \cap \set{C} \right ) .
\end{equation}

Операция объединения дистрибутивна относительно пересечения и наоборот:
\begin{equation}
\set{A} \cup \left( \set{B} \cap \set{C} \right) = \left( \set{A} \cup \set{B} \right) \cap \left( \set{A} \cup \set{C} \right),
\end{equation}
\begin{equation}
\set{A} \cap \left( \set{B} \cup \set{C} \right) = \left( \set{A} \cap \set{B} \right) \cup \left( \set{A} \cap \set{C} \right),
\end{equation}

Кроме того, пересечение множеств дистрибутивно ещё и относительно симметрической разности:
\begin{equation}
\set{A} \cap \left( \set{B} \setsymdiff \set{C} \right) = \left( \set{A} \cap \set{B} \right) \setsymdiff \left( \set{A} \cap \set{C} \right) .
\end{equation}

Разность дистрибутивна относительно объединения только справа:
\begin{equation}
\left( \set{A} \cup \set{B} \right) \setminus \set{C} = \left( \set{A} \setminus \set{C} \right) \cup \left( \set{B} \setminus \set{C} \right) .
\end{equation}

Слева же разность недистирибутивна. Однако этот случай носит название правила де Моргана и важен тем, что позволяет переходить от объединения множеств к их пересечению и обратно:
\begin{equation}
\set{A} \setminus \left( \set{B} \cup \set{C} \right) = \left( \set{A} \setminus \set{B} \right) \cap \left( \set{A} \setminus \set{C} \right),
\end{equation}
\begin{equation}
\set{A} \setminus \left( \set{B} \cap \set{C} \right) = \left( \set{A} \setminus \set{B} \right) \cup \left( \set{A} \setminus \set{C} \right).
\end{equation}

В частности, приняв в последних формулах за \set{A} универсум~\set{U}, можно переписать эти формулы через операцию дополнения:
\begin{equation}
\label{eq:De_Morgan's_laws_not(B u C)}
\overline { \set{B} \cup \set{C} } = \overline{\set{B}} \cap \overline{\set{C}} ,
\end{equation}
\begin{equation}
\label{eq:De_Morgan's_laws_not(B a C)}
\overline { \set{B} \cap \set{C} } = \overline{\set{B}} \cup \overline{\set{C}} .
\end{equation}

Так же важно отметить, что пересечение и объединение множеств с самими собой даёт, то же множество:
\begin{equation}
\label{eq:AuA=A, AaA=A}
\set{A} \cup \set{A} = \set{A}, \qquad \set{A} \cap \set{A} = \set{A},
\end{equation}
а разности "--- нулевое множество
\begin{equation}
\set{A} \setminus \set{A} = \emptyset, \qquad \set{A} \setsymdiff \set{A} = \emptyset,
\end{equation}
что легко понять, просто взглянув на диаграммы Эйлера"--~Венна.


В конце этого параграфа заметим, что, если скобками не определён иной порядок действий, то сперва выполняется дополнение, затем пересечение и, наконец, объединения и разности, имеющие одинаковые приоритеты.