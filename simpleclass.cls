\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{simpleclass}[2014/05/01 Russian math books simple class]
% Данный класс опирается на класс memoir и является его расширением
\LoadClass{memoir}
%%% Базовые пакеты
% Подключение русского языка
\RequirePackage[utf8x]{inputenc}
\RequirePackage[english,russian]{babel}

% Подключение гиперссылок
\RequirePackage{hyperref}

% Подключение математики
\RequirePackage[intlimits,sumlimits,namelimits]{amsmath} % В русской типографике принято ставить пределы и прочее под и над знаком интеграла, суммы и т.п.
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\everymath{\displaystyle} % Выключной формат всем формулам!

%%% Графика
% Подключение графики
\RequirePackage{graphicx}
\graphicspath{{Images/}}
% Использование нескольких картинок в одном рисунке
\RequirePackage[format=plain,labelsep=period,singlelinecheck=false]{caption,subfig}% Нужно для точки после названия.

%%% Вспомогательные команды
\newcommand{\initaldocument}{% Инициализация документа
\mainmatter
\maketitle
\tableofcontents{}
}
